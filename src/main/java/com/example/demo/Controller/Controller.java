package com.example.demo.Controller;

import com.example.demo.Book.Book;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

class Storage
{
    static HashMap<String,Book> db = new HashMap<>();
}

@RestController
class AddBook{
    @PostMapping("/addBook")
    String add(@RequestBody Book book)
    {
        Storage.db.put(book.id,book);
        return "Added";
    }
}
@RestController
class ShowBook{
    @GetMapping("/showBooks")
    HashMap<String,Book> showAll()
    {
       return Storage.db;
    }

    @GetMapping("/showBookById/{id}")
    public Book show(@PathVariable("id") String id)
    {
        return Storage.db.get(id);
    }
}
