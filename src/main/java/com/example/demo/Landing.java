package com.example.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Landing {

    @GetMapping("/")
    public String sayHi()
    {
        return "<h1 style='color : red'>HI</h1>";
    }
}
