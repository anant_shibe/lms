package com.example.demo.Book;

public class Book {
    public String id,name,author;
    public int price;
    public String edition;
    public int bookCount;

    Book(String i, String n, String a, int p, String e, int bc)
    {
        id = i;
        name = n;
        author = a;
        price = p;
        edition = e;
        bookCount = bc;
    }

    Book(){

    }

    @Override
    public String toString() {
        return "Book{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", author='" + author + '\'' +
                ", price=" + price +
                ", edition='" + edition + '\'' +
                ", bookCount=" + bookCount +
                '}';
    }
}
